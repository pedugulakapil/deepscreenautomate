import schedule
import time
import os
import time


class currentID:
    filename = 'build_request_status.txt'

    def setCurrectStatus(self, status:bool):
        file = open(self.filename, 'w')
        file.write(str(status))
        file.close()

    @property
    def getCurrectStatus(self):
        with open(self.filename) as f:
            if f.readline() == "1":
                return True
            print("read failed")
            return False


branchBuild = currentID()

def job():
    print("I'm working...")
    os.system("call python .\DeepScreenBot.py -C")
    os.system("call python .\DailyTask.py")
    branchBuild.setCurrectStatus(1)
    
def stopChecking():
    branchBuild.setCurrectStatus(0)


def startBranchCheck():
    os.system("call python .\main.py")


schedule.every().day.at("01:20").do(stopChecking)
schedule.every().day.at("02:30").do(job)

while True:
    schedule.run_pending()

    if(branchBuild.getCurrectStatus):
        startBranchCheck()

    try:
        time.sleep(60)
    except:
        print("Build Stopped")
        exit()
