import pandas as pd
import os
import csv

onedrive_dir =os.environ['onedrive']

excelSheetPath = r'C:\Ramp\AutoScripts\Projects.xlsx'
buildRequestSheet = f'{onedrive_dir}\Branch Builds\Deepscreen build Request.xlsx'


class currentID:
    filename = 'current_id.txt'

    def setCurrectID(self, id):
        file = open(self.filename, 'w') 
        file.write(str(id)) 
        file.close()

    @property
    def getCurrentID(self):
        with open(self.filename) as f:
            return int(f.readline())


class BuildInterface:
    def __init__(self, build_name, path, config, platform, target, buildtype, status, prefix):
        self.BuildName = build_name
        self.BuildPath = path
        self.BuildConfig = config
        self.BuildPlatform = platform
        self.BuildTarget = target
        self.BuildType = buildtype
        self.BuildStatus = status
        self.BuildPrefix = prefix

class BranchInterface:
    def __init__(self, row_id, build_name, branch, user_name, email, rtosBranch=None, sha_key=None):
        self.RowID = row_id
        self.BuildName = build_name
        self.Branch = branch
        self.UserName = user_name
        self.Email = email
        self.Sha_Key = sha_key
        self.Rtos_Branch = rtosBranch

class checkNewBranchRequest:
    fileName = buildRequestSheet
    BuildFile = pd.ExcelFile(fileName)

    currentid = currentID()

    def __init__(self) -> BranchInterface:
        self.data = self.check_request_list()

    def check_request_list(self):
        sheetName = pd.read_excel(self.fileName, 'Form1')
        ReadSheet = pd.DataFrame(sheetName)

        filterList = ReadSheet.loc[ReadSheet.ID > self.currentid.getCurrentID]

        for index, rows in filterList.iterrows():
            if pd.notnull(rows['RTOS Apps Branch']) :
                RTOSBranch = rows['RTOS Apps Branch']
            else:
                RTOSBranch = None

            return BranchInterface(rows.ID, rows['Build Name'], rows['Branch Name'], rows.Name, rows.Email, RTOSBranch, rows['Commit ID'])
                
        return None