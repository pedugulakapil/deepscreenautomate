import sqlite3
import os
from sqlite3 import Error
from datetime import datetime

sys_user = os.environ['USERNAME']

now = datetime.timestamp(datetime.now())

class ConnectDB:
    def __init__(self):
        if sys_user=="Kapil":
            self.DBName = "Build_History.db"
        elif sys_user=="Deepscreen2":
            self.DBName = "Build_History_Legeto.db"
        else:
            self.DBName = "Build_History-new.db"


        self.checkConnection(self.DBName)
        self.CurrentTime = datetime.fromtimestamp(now).strftime('%m-%d-%Y %I:%M %p')
        


    def checkConnection(self, DB_Name):
        try:
            self.conn = sqlite3.connect(self.DBName)
            print("DB Established Successfully")

            self.conn.cursor()
            self.conn.execute("""CREATE TABLE IF NOT EXISTS Builds (
                Id integer PRIMARY KEY, 
                Build_name text NOT NULL, 
                Branch_name text NOT NULL, 
                SHA_Key text NOT NULL, 
                RTOS_Branch text NOT NULL, 
                Build_path text NOT NULL, 
                User_Name text DEFAULT DailyBuild, 
                Build_Time text NOT NULL, 
                Email_Id text DEFAULT NULL, 
                Build_Status text NOT NULL, 
                Completion_Date real, 
                Submission_Status BOOLEAN DEFAULT FALSE)""")


        except Error as e:  
            print(e)
            self.conn = None


    def addEntry(self, Build_name, Branch_Name, SHA_Key, RTOS_Branch, Build_path, User_Name, Build_Time, Email_Id, Build_Status):
        try:
            self.conn.execute(f"""INSERT INTO Builds (Build_name, Branch_name, SHA_Key, RTOS_Branch, Build_path, User_Name, Build_Time, Email_Id, Build_Status, Completion_Date)
                VALUES ('{Build_name}','{Branch_Name}','{SHA_Key}','{RTOS_Branch}','{Build_path}','{User_Name}','{Build_Time}','{Email_Id}','{Build_Status}','{self.CurrentTime}')""")
            self.conn.commit()
            self.conn.close()
            print('Records inserted')
        except Error as e: 
            print("Records inserttion Failed")
            print(e)


    def getSubmissionList(self):
        try:
            cur = self.conn.cursor()
            cur.execute("SELECT * FROM Builds WHERE Submission_Status=0")
            rows = cur.fetchall()
            cur.close()
            return rows

        except Error as e:
            print("Records Fetching failed")
            print(e)

    def update_task(self, id):
        try:
            sql = f"""Update Builds set Submission_Status = 1 where Id = '{id}'"""
            cur = self.conn.cursor()
            cur.execute(sql)
            self.conn.commit()
            cur.close()
            print(f'Updating row ID = {id}')
        except Error as e:
            print('Updation Failed')
            print(e)

    def closeConnection(self):
            try:
                self.conn.close()
                print("connection Closed")
            except Error as e:
                print('Connection Close Failed')
                print(e)
            
    