import os
import subprocess
from custom_logs import custom_log as log
from git import Repo

class gitCheckouts:
    def __init__(self, path):
        self.buildPath = path
        self.currentDire = os.getcwd()
        self.SHAKey=None
        self.repo = Repo(path, search_parent_directories=True)

    def clean_all_repos(self):
        os.chdir(self.buildPath)
        subprocess.run(["git", "clean", "-dfx"], check=True, stdout=subprocess.PIPE).stdout
        subprocess.run(["git", "reset", "."], check=True, stdout=subprocess.PIPE).stdout
        subprocess.run(["git", "checkout", ".", "-f"], check=True, stdout=subprocess.PIPE).stdout
        subprocess.run(["git", "checkout", "develop", "-f"], check=True, stdout=subprocess.PIPE).stdout

        stdout = subprocess.check_output('git branch'.split())
        out = stdout.decode()
        branches = [b.strip('* ') for b in out.splitlines()]

        for branch in branches:
            if branch != 'develop':
                subprocess.run(["git", "branch", "-d", branch], check=True, stdout=subprocess.PIPE).stdout

        os.chdir(self.currentDire)

    def git_checkout_branch(self, branchName, rtosBranch=None, sha=None, DeepsSreenBranch="master"):
        log.info("checkout out to Build Path")
        os.chdir(self.buildPath)
        self.repo = Repo(self.buildPath, search_parent_directories=True)
        subprocess.run(["git", "clean", "-dfx"], check=True, stdout=subprocess.PIPE).stdout
        subprocess.run(["git", "reset", "."], check=True, stdout=subprocess.PIPE).stdout
        subprocess.run(["git", "checkout", ".", "-f"], check=True, stdout=subprocess.PIPE).stdout
        gitfetch = subprocess.call(["git", "fetch", "--all"], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        log.info("git fetching is Started")

        os.chdir(self.buildPath)

        RepoFetch = False
        while not RepoFetch:
            try:
                self.repo.remotes.origin.fetch()
                RepoFetch = True
                log.info("git fetch completed successfully")
                print("origin fetch completed")
            except:
                print(RepoFetch)
                log.error("git fetch failed")
                log.error("Trying Again to Fetch")

        print("Git Fetch Done")

        if(os.path.exists(f'{self.buildPath}\RtosApps')):   
            os.chdir(f'{self.buildPath}\RtosApps')
            self.repo = Repo('.')
            
            Rtosfetch = False
            while not Rtosfetch:
                try:
                    self.repo.remotes.origin.fetch()
                    Rtosfetch = True
                    log.info("Rtos git fetch completed successfully")
                    print("Rtos origin fetch completed")
                except:
                    log.error("Rtos git fetch failed")
                    log.error("Rtos Trying Again to Fetch")

            print("Rtos Git Fetch Done")

            log.info(f"checking out  RtosApps to {rtosBranch}")
            subprocess.run(["git", "clean", "-dfx"], check=True, stdout=subprocess.PIPE).stdout
            subprocess.run(["git", "reset", "."], check=True, stdout=subprocess.PIPE).stdout
            subprocess.run(["git", "checkout", ".", "-f"], check=True, stdout=subprocess.PIPE).stdout
            
            if rtosBranch:
                isExits = False
                branch_name = f'origin/{rtosBranch}'

                log.info(f"checking out to rtos Branch {rtosBranch}")
                for ref in self.repo.references:
                    if branch_name == ref.name:
                        print(f"Checking out to Rtos branch from develop to {rtosBranch}")
                        log.info(f"Rtos branch name {rtosBranch} is exist")
                        subprocess.run(["git", "checkout", rtosBranch, "-f"], check=True, stdout=subprocess.PIPE).stdout
                        subprocess.run(["git", "pull", "origin", rtosBranch], check=True, stdout=subprocess.PIPE).stdout
                        isExits = True

                if not isExits:
                    log.error(f"Rtos branch name {rtosBranch} does not exist")
                    print(f"Rtos branch name {rtosBranch} does not exist")
                    return False

        os.chdir(f'{self.buildPath}')

        if sha != None and len(str(sha))==40:
            Check_SHA = subprocess.check_output(f"git cat-file -t {sha}")
            if 'commit' in str(Check_SHA):
                log.info(f"{sha} is available")
                subprocess.run(["git", "checkout", sha, "-f"], check=True, stdout=subprocess.PIPE).stdout
                self.SHAKey = subprocess.check_output("git rev-parse --short HEAD")
                log.info(f"Commit ID id is {self.SHAKey}")
                os.chdir(self.currentDire)
                return True
            else:
                log.info("sha checkout failed")
                return False

        tags = branchName.split('/')[0]
        tag_Name = None

        if tags=="tags":
            stdout = subprocess.check_output('git tag'.split())
            tag_Name = branchName.split('/')[1]
        else:
            stdout = subprocess.check_output('git branch -a'.split())
        
        out = stdout.decode()
        branches = [b.strip('* ') for b in out.splitlines()]

        checkout_branch_name = f'remotes/origin/{branchName}'
        log.info(f"checkout out to {checkout_branch_name}")
        
        branchName = branchName.replace(" ", "")

        if checkout_branch_name in branches:
            log.info(f"{branchName} is available")
            log.info(f"checkout to {branchName}")
            print("branch available")
        elif tag_Name in branches:
            log.info(f"tags/{tag_Name} is available")
            log.info(f"checkout to tags/{tag_Name}")
            print("Tag is available")
        else:
            log.error(f"Branch {branchName} not Available")
            print("Branch not available")
            return False

        
        try:
            log.info(f"Cleaning {branchName}")
            subprocess.run(["git", "checkout", branchName, "-f"], check=True, stdout=subprocess.PIPE).stdout
            subprocess.run(["git", "pull", "origin", branchName], check=True, stdout=subprocess.PIPE).stdout
            self.SHAKey = subprocess.check_output("git rev-parse --short HEAD")
            log.info(f"Commit ID id is {self.SHAKey}")

            os.chdir(f'{self.buildPath}\AppFx')
            subprocess.run(["git", "clean", "-dfx"], check=True, stdout=subprocess.PIPE).stdout
            subprocess.run(["git", "reset", "."], check=True, stdout=subprocess.PIPE).stdout
            subprocess.run(["git", "checkout", ".", "-f"], check=True, stdout=subprocess.PIPE).stdout
            subprocess.run(["git", "checkout", "master", "-f"], check=True, stdout=subprocess.PIPE).stdout
            subprocess.run(["git", "pull", "origin", "master"], check=True, stdout=subprocess.PIPE).stdout

            os.chdir(f'{self.buildPath}\AppFx\DeepScreen')
            subprocess.run(["git", "fetch", "--all"], check=True, stdout=subprocess.PIPE).stdout
            subprocess.run(["git", "clean", "-dfx"], check=True, stdout=subprocess.PIPE).stdout
            subprocess.run(["git", "reset", "."], check=True, stdout=subprocess.PIPE).stdout
            subprocess.run(["git", "checkout", ".", "-f"], check=True, stdout=subprocess.PIPE).stdout
            subprocess.run(["git", "checkout", DeepsSreenBranch, "-f"], check=True, stdout=subprocess.PIPE).stdout
            subprocess.run(["git", "pull", "origin", DeepsSreenBranch], check=True, stdout=subprocess.PIPE).stdout
        except:
            return False

        os.chdir(self.currentDire)
        return True

    def getSHAKey(self):
        return self.SHAKey
