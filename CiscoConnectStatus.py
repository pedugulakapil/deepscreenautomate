import os
import time
import pygetwindow as window
import pyautogui as run
import netifaces
import subprocess
from configparser import ConfigParser


class CiscoVPN:
    Login_Config = ConfigParser()
    configPath = "C:\\Ramp\\AutoScripts\\"

    def __init__(self):
        self.Login_Config.read(f'{self.configPath}\\Logins.ini')

    def ConnectionStatus(self):
        brlist = []
        ifaces = [iface for iface in netifaces.interfaces() if iface != 'lo']
        for ifname in ifaces:
            addresses = netifaces.ifaddresses(ifname)
            if netifaces.AF_INET not in addresses:
                continue
            for addr in addresses[netifaces.AF_INET]:
                if 'broadcast' in addr:
                    brlist.append(addr['broadcast'])

        for idx in brlist:
            if str(10) == idx.split(".")[0]:
                return True
        return False

    def CiscoDisconnect(self):
        subprocess.run('cisco-statu.bat', check=True, stdout=subprocess.PIPE).stdout


    def CheckForImg(self, imgName, x, y):
        image = None
        while image is None:
            image = run.locateOnScreen(
                f'{self.configPath}\\Cisco_Login\\{imgName}', confidence=0.9, grayscale=True)
        run.sleep(1)
        run.click(image[0]+x, image[1]+y)
        run.sleep(1)
        return


    def ConnectCisco(self):
        self.path = os.getcwd()
        os.system("taskkill /f /im vpnui.exe")

        run.hotkey("win", "r")
        run.sleep(0.5)
        run.write("C:\\ProgramData\\Microsoft\\Windows\\Start Menu\\Programs\\Cisco\\Cisco AnyConnect Secure Mobility Client\\Cisco AnyConnect Secure Mobility Client.lnk")
        run.hotkey("enter")

        found = None
        while found == None:
            if "Cisco AnyConnect Secure Mobility Client" in window.getAllTitles():
                window.getWindowsWithTitle('Cisco AnyConnect Secure Mobility Client')[
                    0].activate()
                run.sleep(0.5)
                run.hotkey("enter")
                found = True

        # CheckForImg("gm_login.png",302,112)
        self.CheckForImg("Credentials.png", 202, 85)
        run.write(f"{self.Login_Config['CISCO']['GM_ID']}", interval=0.1)
        run.sleep(0.5)
        run.hotkey("Tab")
        run.sleep(0.5)
        run.write(f"{self.Login_Config['CISCO']['GM_PASS']}", interval=0.1)
        run.sleep(0.5)
        run.hotkey("enter")
        run.sleep(7)
        self.CheckForImg("Security_Warning.png", 260, 280)
        self.CheckForImg("Accept_Promt.png", 120, 210)
        run.sleep(5)
        if "GM Posture Check - Google Chrome" in window.getAllTitles():
            self.CheckForImg("Start_btn.png", 10, 5)
