import os
import time
import pygetwindow as window
import pyautogui as run
import netifaces
import subprocess

sys_user = os.environ['USERNAME']

def isGMConnected():
    brlist = []
    ifaces = [iface for iface in netifaces.interfaces() if iface != 'lo']
    for ifname in ifaces:
        addresses = netifaces.ifaddresses(ifname)
        if netifaces.AF_INET not in addresses:
            continue
        for addr in addresses[netifaces.AF_INET]:
            if 'broadcast' in addr:
                brlist.append(addr['broadcast'])

    for idx in brlist:
        if str(10) == idx.split(".")[0]:
            return True
    return False

def CiscoDisconnect():
    subprocess.run('cisco-statu.bat', check=True, stdout=subprocess.PIPE).stdout


if isGMConnected(): CiscoDisconnect()

if sys_user == 'Deepscreen2':
    print("Working on Windows")
    os.system('call .\DeepScreenBot.py -C')
else:
    print("Working on Main")
    os.system('call .\DeepScreenBot.py -C')

time.sleep(5)
os.system('call .\GM_Login\\CiscoConnect.py')
time.sleep(5)

try:
    if isGMConnected():
        print("Cisco Connected")
        if sys_user == 'Kapil':
            os.system('python .\DeepScreenBot.py -QUALCOMM')
        elif sys_user == 'Deepscreen2':
            os.system('start python .\DeepScreenBot.py -Windows')
            os.system('python .\DeepScreenBot.py -NXP')
        else :
            os.system('start python .\DeepScreenBot.py -Windows')
            os.system('python .\DeepScreenBot.py -NXP')

        time.sleep(10)
        CiscoDisconnect()
    else :
        print("Cisco Not Connected")
except:
    print("Issue with CISCO Connection")
