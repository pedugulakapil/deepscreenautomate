import os
import logging
from datetime import datetime

class custom_log:
    onedrive_dir = os.environ['onedrive']
    sys_user = os.environ['USERNAME']

    if sys_user == 'Kapil':
        sys_Name = 'Automation'
    elif sys_user == 'Deepscreen2':
        sys_Name = 'Legeto'
    else:
        sys_Name = 'Local'

    today = datetime.today().strftime('%m_%d_%Y')
    
    if not os.path.isdir(f'{onedrive_dir}\DeepScreen_Cron\Logs'):
        os.mkdir(f'{onedrive_dir}\DeepScreen_Cron\Logs')

    buildLogs = f'{onedrive_dir}\DeepScreen_Cron\Logs\{sys_Name}-{today}.log'
    
    format='[%(asctime)s] %(levelname)-10s %(message)s'

    logging.basicConfig(format=format, datefmt='%m/%d/%Y %I:%M:%S %p', filename=buildLogs, level=logging.INFO)

    def debug(message): logging.debug(message)
    def info(message): logging.info(message)
    def warning(message): logging.warning(message)
    def error(message): logging.error(message)
    def critical(message): logging.critical(message)
    
    