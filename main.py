import pandas as pd
import os
import subprocess
import time
from gitProcess import gitCheckouts
from custom_logs import custom_log as log
import request_sheet as req
from startBuild import startBuild as run
from ConnectDB import ConnectDB as DB

sys_user = os.environ['USERNAME']
excelSheetPath = r'C:\Ramp\AutoScripts\Projects.xlsx'


class currentID:
    filename = 'current_id.txt'

    def setCurrectID(self, id):
        file = open(self.filename, 'w')
        newid = int(id)
        file.write(str(newid))
        file.close()

    @property
    def getCurrentID(self):
        with open(self.filename) as f:
            return int(f.readline())


class getBuildConfig:
    fileName = excelSheetPath
    BuildFile = pd.ExcelFile(fileName)

    def __init__(self, build_name: str) -> req.BuildInterface:
        self.projectName = build_name
        self.config = self.get_build_name()

    def get_build_name(self):
        sheetName = pd.read_excel(self.fileName, sheet_name=0)
        ReadSheet = pd.DataFrame(sheetName)

        for index, rows in ReadSheet.iterrows():
            if rows['Build Name'] == self.projectName:
                return req.BuildInterface(rows['Build Name'], rows.Path, rows.Config, rows.BuildPlatform, rows.Target, rows.BuildType, rows.Status, rows.Prefix)

        return None


if __name__ == '__main__':
    
    requestList = req.checkNewBranchRequest()
    
    log.info("Checking new build request in List")

    item                = requestList.data
    currentid           = currentID()
    buildID             = currentid.getCurrentID
    db                  = None

    if item:
        build = getBuildConfig(item.BuildName).config
        print("Current User ", sys_user)
        print("Build Target ", build.BuildTarget)
        if((sys_user=='Kapil' and (build.BuildTarget=='QUALCOMM')) or (sys_user=='Deepscreen2' and (build.BuildTarget=='NXP' or build.BuildTarget=="Windows" or build.BuildTarget=='QUALCOMM'))):
            log.info(f"found new build request from {item.UserName} for {item.BuildName} branch {item.Branch} ")
            currentid.setCurrectID(item.RowID)
            db                  = DB()
            print("Build Name ", build.BuildName)
            print("Build Target ", build.BuildTarget)
            print("Current User ", sys_user)
            gitActions = gitCheckouts(build.BuildPath)
            checkout_Status = gitActions.git_checkout_branch(item.Branch, item.Rtos_Branch, item.Sha_Key)
		
            if checkout_Status == True:
                log.info(f"running  {item.BuildName} {item.Branch}")
                db.closeConnection()
                run(item.BuildName, item.Branch, item.UserName, item.Email)
            else:
                db.addEntry(item.BuildName, item.Branch, "None", "RTOS Branch", "None", item.UserName, "", item.Email, "Branch Error")
                db.closeConnection()

