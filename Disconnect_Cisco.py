from CiscoConnectStatus import CiscoVPN
import os
import time
import schedule
import subprocess
import ctypes

onedrive_dir = os.environ['onedrive']

class currentStatus:
    filename = f'{onedrive_dir}\\AutomateScript\\VPN_Status.txt'

    def setCurrectID(self, id):
        file = open(self.filename, 'w')
        file.write(str(id))
        file.close()

    @property
    def getCurrentID(self):
        with open(self.filename) as f:
            return f.readline()

VPN = CiscoVPN()
Status = currentStatus()

def CiscoDisconnectIfExist():
    if VPN.ConnectionStatus:
        print("sheduled Disconnecting")
        VPN.CiscoDisconnect()


def CheckFile():
    if Status.getCurrentID=='0':
        print("force Disconnecting")
        VPN.CiscoDisconnect()

schedule.every(5).minutes.do(CheckFile)
schedule.every().day.at("02:19").do(CiscoDisconnectIfExist)


if __name__ == "__main__":
    ctypes.windll.kernel32.SetThreadExecutionState(0x80000002)
    while True:
        try:
            schedule.run_pending()
            time.sleep(1)
        except:
            ctypes.windll.kernel32.SetThreadExecutionState(0x80000000)
            exit()
    
