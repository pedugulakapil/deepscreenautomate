import os
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from ConnectDB import ConnectDB as DB

def fillDailyBuildSheet(rowid, Build, SHA, BuildTime, Status):
    options = Options()
    delay = 15 # seconds
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')  # Last I checked this was necessary.
    driver = webdriver.Chrome('C:\Program Files (x86)\chromedriver.exe', chrome_options=options)

    Build_Name = '/html/body/div/div/div/div/div/div/div[1]/div[2]/div[2]/div[1]/div/div[2]/div/div/input'
    SHA_Key = '/html/body/div/div/div/div/div/div/div[1]/div[2]/div[2]/div[2]/div/div[2]/div/div/input'
    Build_Time = '/html/body/div/div/div/div/div/div/div[1]/div[2]/div[2]/div[3]/div/div[2]/div/div/input'
    Build_Status = '/html/body/div/div/div/div/div/div/div[1]/div[2]/div[2]/div[4]/div/div[2]/div/div/input'
    SubmitBtn = '/html/body/div/div/div/div/div/div/div[1]/div[2]/div[3]/div[1]/button/div'
    
    try:
        driver.get("https://forms.office.com/Pages/ResponsePage.aspx?id=u6DmXeUgCEa-WNVshmt4BRCPZZztADtNurRMXENACK9URVUwUjBDVE4zUUhUMlE2T1g3Q0k0MVpCOC4u")
        WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'form-container')))
        driver.find_element_by_xpath(Build_Name).send_keys(Build)
        driver.find_element_by_xpath(SHA_Key).send_keys(SHA)
        driver.find_element_by_xpath(Build_Time).send_keys(BuildTime)
        driver.find_element_by_xpath(Build_Status).send_keys(Status)
        
        driver.find_element_by_xpath(SubmitBtn).click()
        
        time.sleep(2)
        driver.close()
        
        db.update_task(rowid)

    except TimeoutException:
        print("form filling Failed")

def fillBranchBuildResponse(rowid, Build, Branch, Status, userName, email, path):
    options = Options()
    delay = 15 # seconds
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')  # Last I checked this was necessary.
    driver = webdriver.Chrome('C:\Program Files (x86)\chromedriver.exe', options=options)

    Build_Name          = '/html/body/div/div/div/div/div/div/div[1]/div[2]/div[2]/div[1]/div/div[2]/div/div/input'
    Branch_Name         = '/html/body/div/div/div/div/div/div/div[1]/div[2]/div[2]/div[2]/div/div[2]/div/div/input'
    Build_Status        = '/html/body/div/div/div/div/div/div/div[1]/div[2]/div[2]/div[3]/div/div[2]/div/div/input'
    User_Name           = '/html/body/div/div/div/div/div/div/div[1]/div[2]/div[2]/div[4]/div/div[2]/div/div/input'
    Email_Id            = '/html/body/div/div/div/div/div/div/div[1]/div[2]/div[2]/div[5]/div/div[2]/div/div/input'
    Path                = '/html/body/div/div/div/div/div/div/div[1]/div[2]/div[2]/div[6]/div/div[2]/div/div/input'
    
    SubmitBtn = '/html/body/div/div/div/div/div/div/div[1]/div[2]/div[3]/div[1]/button'
    
    try:
        driver.get("https://forms.office.com/Pages/ResponsePage.aspx?id=u6DmXeUgCEa-WNVshmt4BRCPZZztADtNurRMXENACK9UOExXRVFDM1laOTRPTDczNVVOQlU4VDlXNi4u")
        WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.ID, 'form-container')))
        
        driver.find_element_by_xpath(Build_Name).send_keys(Build)
        driver.find_element_by_xpath(Branch_Name).send_keys(Branch)
        driver.find_element_by_xpath(Build_Status).send_keys(Status)
        driver.find_element_by_xpath(User_Name).send_keys(userName)
        driver.find_element_by_xpath(Email_Id).send_keys(email)
        driver.find_element_by_xpath(Path).send_keys(path)

        driver.find_element_by_xpath(SubmitBtn).click()
        
        time.sleep(2)
        driver.close()

        db.update_task(rowid)
    except:
        print("form filling Failed")

if __name__ == "__main__":
    while True:
        db = DB()
        
        if db.getSubmissionList():
            for item in db.getSubmissionList():
                rowID = item[0]
                Build_name = item[1]
                Branch_name = item[2]	
                SHA_Key = item[3]	
                RTOS_Branch = item[4]	
                Build_path = item[5]	
                User_Name = item[6]	
                Build_Time = item[7]	
                Email_Id = item[8]	
                Build_Status = item[9]	
                Completion_Date = item[10]	
                Submission_Status = item[11]

                try:
                    if User_Name == 'DailyBuild' or Email_Id == 'DailyBuild':
                        fillDailyBuildSheet(rowID, Build_name, SHA_Key, Build_Time, Build_Status)
                    else:
                        fillBranchBuildResponse(rowID, Build_name, Branch_name, Build_Status, User_Name, Email_Id, Build_path)

                except:
                    print("Checking Stopped")
                    exit()
            
        else:
            print("no new data found")
            time.sleep(60)

        db.closeConnection()


