import os
import time
import pygetwindow as window
import pyautogui as run
import netifaces
import subprocess

onedrive_dir =os.environ['onedrive']

def isGMConnected():
    brlist = []
    ifaces = [iface for iface in netifaces.interfaces() if iface != 'lo']
    for ifname in ifaces:
        addresses = netifaces.ifaddresses(ifname)
        if netifaces.AF_INET not in addresses:
            continue
        for addr in addresses[netifaces.AF_INET]:
            if 'broadcast' in addr:
                brlist.append(addr['broadcast'])

    for idx in brlist:
        if str(10) == idx.split(".")[0]:
            return True
    return False

def CiscoDisconnect():
    if isGMConnected() :
        subprocess.run('cisco-statu.bat', check=True, stdout=subprocess.PIPE).stdout



class startBuild:
    def __init__(self, build_name: str, branch_name: str, user_name = 'DailyBuild', user_email = 'DailyBuild'):
        self.Build_Name = build_name
        self.Branch_Name = branch_name
        self.User_Name = user_name
        self.User_Email = user_email
        self.run()

    def run(self):
        time.sleep(5)
        print(os.getcwd())
        os.system(f'call .\GM_Login\CiscoConnect.py')
        time.sleep(5)

        try:
            if isGMConnected():
                print("Cisco Connected")
                print("current direccoory", os.getcwd())
                print("build Name", self.Build_Name, "Branch Name", self.Branch_Name)
                subprocess.run(["python", ".\DeepScreenBot.py", f"-{self.Build_Name}", "-B", self.Branch_Name, "-User", self.User_Name, "-Email", self.User_Email], check=True, stdout=subprocess.PIPE).stdout
                time.sleep(10)
                CiscoDisconnect()
            else :
                print("Cisco Not Connected")
        except:
            print("Issue with CISCO Connection")
            CiscoDisconnect()
