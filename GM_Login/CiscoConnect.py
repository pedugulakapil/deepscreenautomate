import os
import pyautogui as run
import pygetwindow as window
from configparser import ConfigParser

configPath = "C:\\Ramp\\AutoScripts\\"

Login_Config = ConfigParser()
Login_Config.read(f'{configPath}\\Logins.ini')

def CAPSLOCK_STATE():
    import ctypes
    hllDll = ctypes.WinDLL ("User32.dll")
    VK_CAPITAL = 0x14
    return hllDll.GetKeyState(VK_CAPITAL)
    
def CheckForImg(imgName, x, y):
    image = None
    while image is None:
        image = run.locateOnScreen(f'{configPath}\\Cisco_Login\\{imgName}', confidence=0.9, grayscale=True)
    run.sleep(1)
    run.click(image[0]+x, image[1]+y)
    run.sleep(1)
    return


path = os.getcwd()
os.system("taskkill /f /im vpnui.exe")

CAPSLOCK = CAPSLOCK_STATE()
if ((CAPSLOCK) & 0xffff) != 0:
    print("\nWARNING:  CAPS LOCK IS ENABLED!\n")
    run.hotkey("capslock")

run.hotkey("win","r")
run.sleep(0.5)
run.write("C:\\ProgramData\\Microsoft\\Windows\\Start Menu\\Programs\\Cisco\\Cisco AnyConnect Secure Mobility Client\\Cisco AnyConnect Secure Mobility Client.lnk")
run.hotkey("enter")

found = None
while found == None:
    if "Cisco AnyConnect Secure Mobility Client" in window.getAllTitles():
        window.getWindowsWithTitle('Cisco AnyConnect Secure Mobility Client')[0].activate()
        run.sleep(0.5)
        run.hotkey("enter")
        found = True

CheckForImg("Credentials.png",202,85)
run.write(f"{Login_Config['CISCO']['GM_ID']}", interval=0.1)
run.sleep(0.5)
run.hotkey("Tab")
CAPSLOCK = CAPSLOCK_STATE()
if ((CAPSLOCK) & 0xffff) != 0:
    print("\nWARNING:  CAPS LOCK IS ENABLED!\n")
    run.hotkey("capslock")
run.sleep(0.5)
run.write(f"{Login_Config['CISCO']['GM_PASS']}", interval=0.1)
run.sleep(0.5)
run.hotkey("enter")
CheckForImg("Accept_Promt.png",120,210)
run.sleep(5)
if "GM Posture Check - Google Chrome" in window.getAllTitles():
    CheckForImg("Start_btn.png",10,5)
