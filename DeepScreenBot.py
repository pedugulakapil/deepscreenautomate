import argparse
import os
from configparser import ConfigParser
from datetime import date
import pyautogui as run
import pygetwindow as window
import shutil 
from shutil import copytree, Error
import datetime
import subprocess
import threading
import sys
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
import time
from custom_logs import custom_log as log
from ConnectDB import ConnectDB as DB

today = date.today().strftime("%m_%d_%Y")
db_file = 'Build_History.db'
configPath = "C:\\Ramp\\AutoScripts\\"
sys_user = os.environ['USERNAME']

startTime = 0
endTime = 0

Login_Config = ConfigParser()
Login_Config.read(f'{configPath}\\Logins.ini')

oneDriveBuildPath = Login_Config['ONEDRIVE']['path']

ProjectConfig = ConfigParser()
ProjectConfig.read(f'{configPath}\\Projects.ini')

oneDrivePath = Login_Config['ONEDRIVE']['path']

class startCheck(threading.Thread):
    status = True
    errorType = "Done"

    def __init__(self, threadID, name, counter):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.counter = counter

    def getErrorData(self):
        return self.errorType

    def stop(self):
        self.status = False

    def run(self):
        errorsList = ["DeepScreen version checker: ERROR", "Altia Error", "Parser error", "Flexible License Manager", "Image Size Mismatch Report"]
        print("Starting " + self.name)
        while self.status:
            for item in window.getAllTitles():
                if item in errorsList:
                    time.sleep(1)
                    try:
                        window.getWindowsWithTitle(item)[0].activate()
                        run.hotkey("enter")
                    except:
                        print("window Erro")
                    self.errorType = item
                    time.sleep(1)
        print("Exiting " + self.name)

def fillExcelSheet(Build, SHA, Status, BuildTime):
    options = Options()
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')  # Last I checked this was necessary.
    driver = webdriver.Chrome('C:\Program Files (x86)\chromedriver.exe', chrome_options=options)

    Build_Name = '//*[@id="form-container"]/div/div/div[1]/div/div[1]/div[2]/div[2]/div[1]/div[2]/div[3]/div/div/div/input'
    SHA_Key = '//*[@id="form-container"]/div/div/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[2]/div[3]/div/div/div/input'
    Build_Time = '//*[@id="form-container"]/div/div/div[1]/div/div[1]/div[2]/div[2]/div[3]/div[2]/div[3]/div/div/div/input'
    Build_Status = '//*[@id="form-container"]/div/div/div[1]/div/div[1]/div[2]/div[2]/div[4]/div[2]/div[3]/div/div/div/input'
    SubmitBtn = '//*[@id="form-container"]/div/div/div[1]/div/div[1]/div[2]/div[3]/div/div/button'
    
    try:
        driver.get("https://forms.office.com/Pages/ResponsePage.aspx?id=u6DmXeUgCEa-WNVshmt4BRCPZZztADtNurRMXENACK9URVUwUjBDVE4zUUhUMlE2T1g3Q0k0MVpCOC4u")
        print(Build, SHA, BuildTime, Status)
        driver.find_element_by_xpath(Build_Name).send_keys(Build)
        driver.find_element_by_xpath(SHA_Key).send_keys(SHA)
        driver.find_element_by_xpath(Build_Time).send_keys(BuildTime)
        driver.find_element_by_xpath(Build_Status).send_keys(Status)
        driver.find_element_by_xpath(SubmitBtn).click()
        time.sleep(5)
        driver.close()
    except NoSuchElementException:
        print("form filling Failed")

def CleanRepo(path, branchName = 'develop', DeepScreenBranch = 'master', AppFxBranch = 'master', RtosAppsBranch = None):
    log.info(f"Cleaning Repo")
    os.system('git reset .')
    os.system('git reset --hard')
    os.system('git checkout . -f')
    os.system('git clean -dfx')
    os.system('git submodule update')

    os.chdir(f'{path}/AppFx')
    os.system('git reset .')
    log.info(f"Cleaning AppFx")
    os.system('git checkout . -f')
    os.system(f'git checkout {AppFxBranch}')
    os.system('git clean -dfx')

    os.chdir(f'{path}/AppFx/DeepScreen')
    log.info(f"Cleaning Deepscreen")
    os.system('git reset .')
    os.system('git checkout . -f')
    os.system(f'git checkout {DeepScreenBranch}')
    os.system('git clean -dfx')

    os.chdir(path)

def GitClean(path, branchName = 'develop', DeepScreenBranch = 'master', AppFxBranch = 'master', RtosAppsBranch = None):
    log.info(f"Repo Checking out to {branchName}")
    os.system('git reset .')
    os.system('git reset --hard')
    os.system('git fetch --all')
    os.system('git checkout . -f')
    os.system('git clean -dfx')
    os.system('git submodule update --init --recursive')
    os.system(f'git checkout {branchName} -f')
    os.system(f'git pull origin {branchName} -f')
    os.system('git submodule update')

    os.chdir(f'{path}/AppFx')
    log.info(f"AppFx branch checkout to {AppFxBranch}")
    os.system('git reset .')
    os.system('git checkout . -f')
    os.system(f'git checkout {AppFxBranch}')
    os.system('git clean -dfx')
    os.system(f'start git pull origin {AppFxBranch}')

    os.chdir(f'{path}/AppFx/DeepScreen')
    log.info(f"Deepscreen branch checkout to {DeepScreenBranch}")
    os.system('git reset .')
    os.system('git checkout . -f')
    os.system(f'git checkout {DeepScreenBranch} -f')
    os.system('git clean -dfx')
    os.system(f'git pull origin {DeepScreenBranch}')

    os.chdir(path)

def GetSHAKey():
    os.system('git rev-parse HEAD > commitid.txt')
    f = open('commitid.txt', 'r')
    line = f.readline()
    f.close()
    os.system('del commitid.txt')
    return os.linesep.join([s for s in line.splitlines() if s])

def CopyFiles(projectName, ProPath, target, status, branch, UserName, UserEmail, db):
    SHAKey          = GetSHAKey()
    endTime         = datetime.datetime.now().replace(microsecond=0)
    oneDrivePath    = Login_Config['ONEDRIVE']['path']
    branchName      = branch.replace("/", "-")
    rhmiPath        = f'{ProPath}\\AppFx\\DeepScreen\\rhmi_gm'
    Buildsucceed    = False
    TimeCalc        = str(endTime - startTime)
    
    if branch == 'develop' :
        BuildBranch = SHAKey
    else :
        BuildBranch = str(f'{branch}-{SHAKey}')

    for path, dirs, files in os.walk(rhmiPath):
        if ("GmXmlDeepScreen_Debug.lib" in files) or ("libAltiaLibSingle.a" in files):
            Buildsucceed = True


    buildPath = None

    if Buildsucceed==True:
        os.system(f'C:\\Ramp\\AutoScripts\\CopyScript.bat {ProPath} {target} {projectName} {today} {SHAKey} {branchName} {oneDrivePath}')

        if branchName == 'develop': 
            buildPath = f'{target}\{projectName}\{today}\{SHAKey}.7z'
        else:
            buildPath = f'Branches\{branchName}\{target}\{projectName}\{today}\{SHAKey}.7z'

        log.info(f'{projectName} {BuildBranch} {status} {TimeCalc}')
    else:
        print(f'-----------------------Build Failed {projectName}---------------------------')
        if status == "Done":
            log.info(f'{projectName} {BuildBranch} Build Failed, {TimeCalc}')
            status = "Build Failed"
        else:
            log.info(f'{projectName} {BuildBranch} {status} {TimeCalc}')

        if(os.path.exists(f'{ProPath}\{projectName}-{today}.txt')):
            try:
                os.makedirs(f'{oneDrivePath}\\Errors\\{today}\\{projectName}',exist_ok=True)
                shutil.copy(f'{ProPath}\{projectName}-{today}.txt', f'{oneDrivePath}\\Errors\\{today}\\{projectName}\\error-{branchName}-{SHAKey}.txt')
                os.remove(f'{ProPath}\{projectName}-{today}.txt')
            except Error as err:
                print(err.args[0])
                log.critical("Error Copy Failed")
                print("Error Copy failed")

        buildPath = f'Errors\{today}-new\{projectName}\error-{branch}-{SHAKey}.txt'

    if db:
        try:
            db.addEntry(projectName, branch, SHAKey, "develop", buildPath, UserName, TimeCalc, UserEmail, status)
        except:
            print("DB Insertion Failed")


def StartBuild(project, UserName, UserEmail):
    if ProjectConfig[project]['status'] == 'Active' :
        projectPath     = ProjectConfig[project]['path']
        target          = ProjectConfig[project]['target']
        configType      = ProjectConfig[project]['config']
        buildType       = ProjectConfig[project]['buildType']
        buildPlatform   = ProjectConfig[project]['BuildPlatform']

        db              = None
        if not UserName == UserEmail:
            db              = DB()

        print ('working on', project, projectPath)
        log.info(f"{projectPath} redirected to")
        os.chdir(projectPath)

        if results.GitClean: 
            if (results.BranchName) and (results.BuildPlatFoarm[0] is project): 
                log.info(f"git cleaning started for {results.BranchName[0]} - {project}")
                GitClean(projectPath, results.BranchName[0])
            else:
                log.info(f"git cleaning started for {project}")
                GitClean(projectPath, 'develop')
                log.info(f"git cleaning completed for {project}")
            return

        BuildStatusMessage = "Done"
        CleanRepo(projectPath,'develop')

        thread1 = startCheck(1, project, 1)
        thread1.start()
        log.info(f"DeepSCreen Build Started for {project}")

        if (ProjectConfig[project]['target']=='Windows') :
            os.system(f'AppFx\Scripts\Build_DeepScreen.bat {buildPlatform} {configType} >> {projectPath}/{project}-{today}.txt 2>&1')
        else:
            if sys_user=='Deepscreen2':
                os.system(f'AppFx\Scripts\Build_DeepScreen.bat {buildPlatform} {target} {configType} {buildType} noLicense >> {projectPath}/{project}-{today}.txt 2>&1')
            else:
                os.system(f'AppFx\Scripts\Build_DeepScreen.bat {buildPlatform} {target} {configType} {buildType} >> {projectPath}/{project}-{today}.txt 2>&1')

        log.info(f"DeepSCreen Build Completed for {project}")
        
        if (thread1.getErrorData() != "Done") : BuildStatusMessage = thread1.getErrorData()
        thread1.stop()

        
        log.info(f"Starting Copying {project}")
        if results.BranchName == None : 
            CopyFiles(project, projectPath, buildPlatform, BuildStatusMessage, 'develop', UserName, UserEmail, db)
        else : 
            CopyFiles(project, projectPath, buildPlatform, BuildStatusMessage, results.BranchName[0],  UserName, UserEmail, db)
        log.info(f"Copying {project} Completed")
        
        
        

if __name__ == "__main__":

    prifixList = []
    UserName = 'DailyBuild'
    UserEmail = 'DailyBuild'
    parser = argparse.ArgumentParser(prog='PROG')
    parser.add_argument('-C',           action='store_true',      dest='GitClean',          help='Cleans Repos',                    default=False)
    parser.add_argument('-Windows',     action='append_const',    dest='BuildPlatFoarm',    help='Build Windows DeepScreen',        const='Windows')
    parser.add_argument('-NXP',         action='append_const',    dest='BuildPlatFoarm',    help='Build NXP DeepScreen',            const='NXP')
    parser.add_argument('-QUALCOMM',    action='append_const',    dest='BuildPlatFoarm',    help='Build QUALCOMM DeepScreen',       const='QUALCOMM')
    parser.add_argument('-B',           action='append',          dest='BranchName',        help='Change Defalt Branch Name')
    parser.add_argument('-User',        action='append',          dest='UserName',          help='Name of the user who requested for Build')
    parser.add_argument('-Email',       action='append',          dest='UserEmail',         help='Email of the user who requested for Build')
    
    for project in ProjectConfig.sections() :
        if f'{ProjectConfig[project]["prifix"]}' not in prifixList :
            parser.add_argument(f'-{ProjectConfig[project]["prifix"]}',  
                                action='append_const',    
                                dest='BuildPrefix',    
                                help=f'to Build {ProjectConfig[project]["prifix"]} App',
                                const=f'{ProjectConfig[project]["prifix"]}')
            prifixList.append(f'{ProjectConfig[project]["prifix"]}')
    
    for project in ProjectConfig.sections() :
        if f'{ProjectConfig[project]["status"]}' == 'Active':
            parser.add_argument(f'-{project}',          
                                action='append_const',    
                                dest='BuildPlatFoarm',    
                                const=f'{project}')

    
    try:
        results = parser.parse_args()
        if results.UserName : UserName = results.UserName[0]
        if results.UserEmail : UserEmail = results.UserEmail[0]

    except SystemExit:
        print("No Arguments Passed")
        exit()


    if results.BuildPlatFoarm == None and results.BuildPrefix == None and results.GitClean :
        mylist = list()
        for project in ProjectConfig.sections() : 
            mylist.append(ProjectConfig[project]['path'])
        mylist = list(dict.fromkeys(mylist))
        for item in mylist : 
            os.chdir(item)
            log.info(f"Git Cleaning started for {project}")
            GitClean(item)
        exit()

    for project in ProjectConfig.sections() :
        startTime = datetime.datetime.now().replace(microsecond=0)
        if (results.BuildPlatFoarm and (results.BuildPlatFoarm[0] == ProjectConfig[project]['target'])) and (results.BuildPrefix and (results.BuildPrefix[0] == ProjectConfig[project]['prifix'])):
            log.info(f"Build Starting {project}")
            StartBuild(project, UserName, UserEmail)
            log.info(f"{project} Build Completed")
        elif (
                ((results.BuildPlatFoarm and results.BuildPrefix == None)   and (results.BuildPlatFoarm[0]  == ProjectConfig[project]['target'])) or 
                (results.BuildPlatFoarm and (results.BuildPlatFoarm[0]  == project)) or 
                ((results.BuildPrefix and results.BuildPlatFoarm == None)   and (results.BuildPrefix[0]     == ProjectConfig[project]['prifix']))
            ) :
            log.info(f"Build Starting {project}")
            StartBuild(project, UserName, UserEmail)
            log.info(f"{project} Build Completed")
